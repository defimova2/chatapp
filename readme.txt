SETUP
----------------------

1. Install py packages from requirements.txt:

$ pip install falcon
$ pip install gunicorn
$ pip install sqlalchemy

2. Database is SQLite3:

$ sudo apt install sqlite3

3. Install and run Redis:

1) Installation:
$ wget http://download.redis.io/redis-stable.tar.gz
$ tar xzf redis-stable.tar.gz
$ cd redis-stable
$ make

2) Run redis server:
$ src/redis-server

3) Install redis-py:

$ pip install redis

4. Run gunicorn:

gunicorn --reload chat.app

5. Nginx config (/etc/nginx/sites-available/chat_app.conf):

server {
	listen 80;
	server_name localhost;
	root /home/defimova/progs/python/web/projects/falcon/chat/chat/templates;
        expires 0;

	location /api/v1 {
		include proxy_params;
		proxy_pass http://localhost:8000;
	}
        location ~* (.*).html$ {
		expires 0;
	}
}


HOW TO USE APPLICATION
----------------------

1. In a browser tab, enter the following URL:
localhost/login.html

The following pre-defined users are available:
login           password     user name
a1@gmail.com    a1           Иван Иванов
b1@gmail.com    b1           Анна Анина
c1@gmail.com    c1           Катя Катина
d1@gmail.com    d1           Егор Егоров
e1@gmail.com    e1           Александр
f1@gmail.com    f1           Александр Алексеев
g1@gmail.com    g1           Александр Сашин
i1@gmail.com    i1           Саша Сашин
j1@gmail.com    j1           Nastya Nastina
h1@gmail.com    h1           Дина Ефимова

Log in as h1.

2. In a different browser tab, enter localhost/login.html and log in as j1.

3. For a user who was logged-in, the application lists a set of chats under "Chats" section.
A chat between two people (the logged-in user and another user) is named after that different user.
A chat between multiple people has its own name. Example: "CRS".

Participants of "CRS" chat are:

login           password     user name
a1@gmail.com    a1           Иван Иванов
b1@gmail.com    b1           Анна Анина
e1@gmail.com    e1           Александр
j1@gmail.com    j1           Nastya Nastina
h1@gmail.com    h1           Дина Ефимова

4. By clicking on a specific chat, the logged-in user sees a list of messages from that chat under "Messages" section.

A new message can be added by entering a text into Textarea and pressing "Add" button (Enter doesn't work).

The rest of chat participants can see new messages by refreshing the page.

To be done: For group chats, list chat participants and the time they were online (performed a call to API) for the last time.


