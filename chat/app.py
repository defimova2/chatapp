import falcon

from .resources import LoginResource, ChatResource, MessageResource

api = application = falcon.API()
api.req_options.auto_parse_form_urlencoded = (
    True
)  # makes the form parameters accessible via params, get_param()

login = LoginResource()
chats = ChatResource()
messages = MessageResource()

api.add_route("/api/v1/login", login)
api.add_route("/api/v1/chats", chats)
api.add_route("/api/v1/chats/{chat_id}/messages", messages)
