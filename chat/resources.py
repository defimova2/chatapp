import datetime
import random
import string

import falcon
import json
import redis

from .models import Contact, Chat, ChatParticipant, Message, get_db_session

from sqlalchemy import and_
from sqlalchemy.orm import aliased

db_session = get_db_session()

redis_host = "localhost"
redis_port = 6379
redis_password = ""

rds = redis.StrictRedis(
    host=redis_host, port=redis_port, password=redis_password, decode_responses=True
)


class LoginResource:
    def on_post(self, req, resp):
        try:
            username = req.params["username"]
            password = req.params["pwd"]
            try:
                db_users = (
                    db_session.query(Contact.contact_id)
                    .filter(
                        and_(Contact.login == username, Contact.password == password)
                    )
                    .one_or_none()
                )

                if db_users:
                    token_id = "".join(
                        random.choice(
                            string.ascii_uppercase
                            + string.ascii_lowercase
                            + string.digits
                        )
                        for x in range(16)
                    )
                    rds.set("users:%s" % token_id, str(db_users[0]))

                    resp.status = falcon.HTTP_200
                    resp.body = json.dumps(
                        {
                            "token_id": token_id,
                            "contact_id": rds.get("users:%s" % token_id),
                        },
                        ensure_ascii=False,
                    )
                else:
                    resp.status = falcon.HTTP_401
            except:
                resp.status = falcon.HTTP_404
        except:
            resp.status = falcon.HTTP_401


def is_authorized(client_token_id, client_contact_id):
    server_contact_id = rds.get("users:%s" % client_token_id)
    if not server_contact_id:
        return False
    elif server_contact_id != client_contact_id:
        return False
    else:
        return True


class ChatResource:
    def get_chats(self, req, resp, contact_id):
        chats = []

        cp = aliased(ChatParticipant)
        cp2 = aliased(ChatParticipant)

        db_chats = (
            db_session.query(
                Chat.chat_id, Chat.chat_name, Contact.first_name, Contact.last_name
            )
            .join(cp, Chat.chat_id == cp.chat_id)
            .filter(cp.contact_id == contact_id)
            .outerjoin(
                cp2,
                and_(
                    cp.chat_id == cp2.chat_id,
                    and_(Chat.is_multicontact == 0, cp.contact_id != cp2.contact_id),
                ),
            )
            .outerjoin(Contact, cp2.contact_id == Contact.contact_id)
            .order_by(Chat.last_message_dtm.desc())
        )

        for r in db_chats:
            cn = ""
            if r.chat_name:
                cn = r.chat_name
            elif r.first_name and r.last_name:
                cn = r.first_name + " " + r.last_name
            elif r.first_name:
                cn = r.first_name
            elif r.last_name:
                cn = r.last_name
            else:
                cn = "Unknown"
            dc = {"chat_id": r.chat_id, "chat_name": cn}

            chats.append(dc)

        return chats

    def on_get(self, req, resp):
        if not is_authorized(req.params["token_id"], req.params["contact_id"]):
            resp.status = falcon.HTTP_401
        else:
            chats = self.get_chats(req, resp, req.params["contact_id"])
            resp.status = falcon.HTTP_200
            resp.body = json.dumps([ob for ob in chats], ensure_ascii=False)


class MessageResource:
    def get_messages(self, req, resp, contact_id, chat_id):
        messages = []

        db_messages = (
            db_session.query(
                Message.message,
                Message.message_dtm,
                Contact.first_name,
                Contact.last_name,
            )
            .filter(Message.chat_id == chat_id)
            .join(ChatParticipant, Message.chat_id == ChatParticipant.chat_id)
            .filter(ChatParticipant.contact_id == contact_id)
            .join(Contact, Message.contact_id == Contact.contact_id)
            .order_by(Message.message_dtm.desc())
            .limit(10)
            .from_self()
            .order_by(Message.message_dtm)
        )

        for r in db_messages:
            cn = ""
            if r.first_name and r.last_name:
                cn = r.first_name + " " + r.last_name
            elif r.first_name:
                cn = r.first_name
            elif r.last_name:
                cn = r.last_name
            else:
                cn = "Unknown"
            dc = {
                "message": r.message,
                "message_dtm": r.message_dtm,
                "contact_name": cn,
            }

            messages.append(dc)

        return messages

    def on_get(self, req, resp, chat_id):
        if not is_authorized(req.params["token_id"], req.params["contact_id"]):
            resp.status = falcon.HTTP_401
        else:
            messages = self.get_messages(req, resp, req.params["contact_id"], chat_id)
            resp.status = falcon.HTTP_200
            resp.body = json.dumps([ob for ob in messages], ensure_ascii=False)

    def on_post(self, req, resp, chat_id):
        if not is_authorized(req.params["token_id"], req.params["contact_id"]):
            resp.status = falcon.HTTP_401
        else:
            m = Message(
                chat_id=chat_id,
                contact_id=req.params["contact_id"],
                message_dtm=datetime.datetime.now(),
                message=req.params["message"],
            )
            db_session.add(m)
            db_session.commit()
            resp.status = falcon.HTTP_201
