from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

engine = create_engine("sqlite:///chat/chat.db", echo=True)
Session = sessionmaker(bind=engine)
db_session = Session()


def get_db_session():
    return db_session


Base = declarative_base()


class Contact(Base):
    __tablename__ = "contacts"

    contact_id = Column(Integer, primary_key=True)
    login = Column(String)
    password = Column(String)
    first_name = Column(String)
    last_name = Column(String)

    def __repr__(self):
        return (
            "<Contact(contact_id = '%s', login='%s', password='%s', first_name='%s', last_name='%s')>"
            % (
                self.contact_id,
                self.login,
                self.password,
                self.first_name,
                self.last_name,
            )
        )


class Chat(Base):
    __tablename__ = "chats"

    chat_id = Column(Integer, primary_key=True)
    is_multicontact = Column(Integer)
    chat_name = Column(String)
    created_dtm = Column(String)
    last_message_dtm = Column(String)

    def __repr__(self):
        return (
            "<Chat(chat_id='%s', is_multicontact='%s', chat_name='%s', created_dtm='%s', last_message_dtm='%s')>"
            % (
                self.chat_id,
                self.is_multicontact,
                self.chat_name,
                self.created_dtm,
                self.last_message_dtm,
            )
        )


class ChatParticipant(Base):
    __tablename__ = "chat_participants"

    chat_id = Column(Integer, primary_key=True)
    contact_id = Column(Integer, primary_key=True)
    created_dtm = Column(String)

    def __repr__(self):
        return "<ChatParticipant(chat_id='%s', contact_id='%s', created_dtm='%s')>" % (
            self.chat_id,
            self.contact_id,
            self.created_dtm,
        )


class Message(Base):
    __tablename__ = "messages"

    message_id = Column(Integer, primary_key=True)
    chat_id = Column(Integer)
    contact_id = Column(Integer)
    message_dtm = Column(String)
    message = Column(String)

    def __repr__(self):
        return (
            "<Message(message_id='%s', chat_id='%s', contact_id='%s', message_dtm='%s', message='%s')>"
            % (
                self.message_id,
                self.chat_id,
                self.contact_id,
                self.message_dtm,
                self.message,
            )
        )


Base.metadata.create_all(engine)
