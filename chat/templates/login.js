var main = function () {
  "use strict";
  $("#failed-login").hide();
};
$(document).ready(main);

$("#btn").click(function() {
  $.ajax({
    url: '/api/v1/login',
    type: 'POST',
    data:({
        username: $("#username").val(),
        pwd: $("#pwd").val()
    }),
    success:function(results) {
        $("#failed-login").text(results.token_id + " , " + results.contact_id);
        $("#failed-login").show();
        window.location.replace("chat.html?token_id=" + results.token_id + '&contact_id=' + results.contact_id);
    },
    error:function (jqXHR, textStatus, errorThrown) {
        if (errorThrown === "Unauthorized")
        {
            $("#failed-login").text("Неверный логин или пароль");
        }  
        else
        {
            $("#failed-login").text("Ошибка на сервере. Невозможно выполнить аутентификакцию");
        }
        $("#failed-login").show();
    }
  });
});