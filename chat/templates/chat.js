var tmp = new Array();      // два вспомагательных
var tmp2 = new Array();     // массива
var param = new Array();

var get = location.search;  // строка GET запроса, то есть все данные после ?
if(get != '') {
  tmp = (get.substr(1)).split('&');   // разделяем переменные
  for(var i=0; i < tmp.length; i++) {
      tmp2 = tmp[i].split('=');       // массив param будет содержать
      param[tmp2[0]] = tmp2[1];       // пары ключ(имя переменной)->значение
  }
}

function getMessages(chat_id) {
  if (param["chat_id"] == chat_id) {
  }
  else {
    var url = new URL(location);
    var query_string = url.search;
    var search_params = new URLSearchParams(query_string); 
    search_params.set('chat_id', chat_id);
    url.search = search_params.toString();
    location = url.toString();
  }

  $.ajax({
    url: "/api/v1/chats/" + chat_id + "/messages",
    type: 'GET',
    data:({
        token_id: param["token_id"],
        contact_id: param["contact_id"]
    }),
    success:function(results) {
        $(".messages").empty();
        $(".messages").append($("<p>Сообщения</p>"));
        for (var i=0; i < results.length; i++) {
            var p = document.createElement('p');
            var linkText = document.createTextNode("[" + results[i].contact_name + "] [" + results[i].message_dtm + "]   " + results[i].message);
            p.appendChild(linkText);
            $(".messages").append(p);
        }
        $("#current-chat").text(chat_id);
        $("#ajaxform").show();
    }
  });
};

var main = function () {
  $.ajax({
    url: '/api/v1/chats',
    type: 'GET',
    data:({
        token_id: param["token_id"],
        contact_id: param["contact_id"]
    }),
    success:function(results) {
        for (var i=0; i < results.length; i++) {
            var btn = document.createElement('button');
            var linkText = document.createTextNode(results[i].chat_name);
            btn.appendChild(linkText);
            btn.id = i;
            btn.type = "button";
            btn.value = results[i].chat_name;
            btn.setAttribute('onclick', 'getMessages(' + results[i].chat_id + ')');
            $(".chats").append(btn);
            $(".chats").append($("<br>"));
        }
    },
    error:function (jqXHR, textStatus, errorThrown) {
        window.location.replace("login.html");
    }
  });
  if (param["chat_id"] !== null) {
    getMessages(param["chat_id"]);
  }
};
$(document).ready(main);


$("#addmbtn").click(function() {
  $.ajax({
    url: "/api/v1/chats/" + $("#current-chat").text() + "/messages",
    type: 'POST',
    data:({
        token_id: param["token_id"],
        contact_id: param["contact_id"],
        message: $("#message").val()
    })
  });
  getMessages($("#current-chat").text());
  document.getElementById("message").value = "";
});
