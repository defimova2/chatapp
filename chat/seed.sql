create table contacts (
	contact_id INTEGER NOT NULL, 
	login VARCHAR NOT NULL, 
	password VARCHAR NOT NULL, 
	first_name VARCHAR NOT NULL, 
	last_name VARCHAR NOT NULL, 
	PRIMARY KEY (contact_id)
);

create table chats (
	chat_id INTEGER NOT NULL,
	is_multicontact INTEGER NOT NULL,
	chat_name TEXT, 
	created_dtm TEXT NOT NULL,
	last_message_dtm TEXT NOT NULL,
	PRIMARY KEY (chat_id)
);

create table chat_participants (
	chat_id INTEGER NOT NULL, 
	contact_id INTEGER NOT NULL,
	created_dtm TEXT NOT NULL,
	PRIMARY KEY (chat_id, contact_id)
);

create table messages (
	message_id INTEGER PRIMARY KEY AUTOINCREMENT,
	chat_id INTEGER NOT NULL,
	contact_id INTEGER NOT NULL, 
	message_dtm TEXT NOT NULL, 
	message TEXT NOT NULL
);


insert into contacts values(1, 'a1@gmail.com', 'a1', 'Иван', 'Иванов');
insert into contacts values(2, 'b1@gmail.com', 'b1', 'Анна', 'Анина');
insert into contacts values(3, 'c1@gmail.com', 'c1', 'Катя', 'Катина');
insert into contacts values(4, 'd1@gmail.com', 'd1', 'Егор', 'Егоров');
insert into contacts values(5, 'e1@gmail.com', 'e1', 'Александр', '');
insert into contacts values(6, 'f1@gmail.com', 'f1', 'Александр', 'Алесеев');
insert into contacts values(7, 'g1@gmail.com', 'g1', 'Александр', 'Сашин');
insert into contacts values(8, 'i1@gmail.com', 'i1', 'Саша', 'Сашин');
insert into contacts values(9, 'j1@gmail.com', 'j1', 'Nastya', 'Nastina');
insert into contacts values(10, 'h1@gmail.com', 'h1', 'Дина', 'Ефимова');

insert into chats values(1, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(2, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(3, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(4, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(5, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(6, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(7, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(8, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(9, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(10, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(11, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(12, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(13, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(14, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(15, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(16, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(17, 0, NULL, '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');
insert into chats values(18, 1, 'CRS', '2018-12-01 17:35:16.000', '1900-01-01 00:00:00.000');

insert into chat_participants values(1, 1, '2018-12-01 17:35:16.000');
insert into chat_participants values(1, 2, '2018-12-01 17:35:16.000');
insert into chat_participants values(2, 1, '2018-12-01 17:35:16.000');
insert into chat_participants values(2, 5, '2018-12-01 17:35:16.000');
insert into chat_participants values(3, 1, '2018-12-01 17:35:16.000');
insert into chat_participants values(3, 6, '2018-12-01 17:35:16.000');
insert into chat_participants values(4, 2, '2018-12-01 17:35:16.000');
insert into chat_participants values(4, 3, '2018-12-01 17:35:16.000');
insert into chat_participants values(5, 2, '2018-12-01 17:35:16.000');
insert into chat_participants values(5, 4, '2018-12-01 17:35:16.000');
insert into chat_participants values(6, 9, '2018-12-01 17:35:16.000');
insert into chat_participants values(6, 1, '2018-12-01 17:35:16.000');
insert into chat_participants values(7, 9, '2018-12-01 17:35:16.000');
insert into chat_participants values(7, 4, '2018-12-01 17:35:16.000');
insert into chat_participants values(8, 9, '2018-12-01 17:35:16.000');
insert into chat_participants values(8, 5, '2018-12-01 17:35:16.000');
insert into chat_participants values(9, 9, '2018-12-01 17:35:16.000');
insert into chat_participants values(9, 8, '2018-12-01 17:35:16.000');
insert into chat_participants values(10, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(10, 1, '2018-12-01 17:35:16.000');
insert into chat_participants values(11, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(11, 2, '2018-12-01 17:35:16.000');
insert into chat_participants values(12, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(12, 3, '2018-12-01 17:35:16.000');
insert into chat_participants values(13, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(13, 4, '2018-12-01 17:35:16.000');
insert into chat_participants values(14, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(14, 5, '2018-12-01 17:35:16.000');
insert into chat_participants values(15, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(15, 6, '2018-12-01 17:35:16.000');
insert into chat_participants values(16, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(16, 8, '2018-12-01 17:35:16.000');
insert into chat_participants values(17, 10, '2018-12-01 17:35:16.000');
insert into chat_participants values(17, 9, '2018-12-01 17:35:16.000');
insert into chat_participants values(18, 1, '2018-12-01 17:35:16.000');
insert into chat_participants values(18, 2, '2018-12-01 17:35:16.000');
insert into chat_participants values(18, 5, '2018-12-01 17:35:16.000');
insert into chat_participants values(18, 9, '2018-12-01 17:35:16.000');
insert into chat_participants values(18, 10, '2018-12-01 17:35:16.000');

insert into messages values(1, 17, 9, '2018-12-03 17:35:16.000', 'Привет. Как дела?');
insert into messages values(2, 17, 10, '2018-12-03 17:37:12.000', 'Привет. Хорошо :)');
insert into messages values(3, 17, 10, '2018-12-03 17:38:01.000', 'А у тебя как? Что нового?');

/*

-- get chats per user
select cp.chat_id,
	coalesce(ch.chat_name, rtrim(c.first_name || ' ' ||  c.last_name)) as chat_name
from chat_participants cp
join chats ch on ch.chat_id = cp.chat_id
left join chat_participants cp2 on cp2.chat_id = cp.chat_id and ch.is_multicontact = 0 and cp2.contact_id <> cp.contact_id
left join contacts c on c.contact_id = cp2.contact_id
where cp.contact_id = 10
order by ch.last_message_dtm desc, chat_name

-- get messages per chat and user
select m.message, m.message_dtm, c.first_name, c.last_name
from messages m
join chat_participants cp on cp.chat_id = m.chat_id
join contacts c on c.contact_id = m.contact_id
where m.chat_id = 17 and cp.contact_id = 10
order by m.message_dtm

*/